module PM_Base_class

#include "petsc/finclude/petscts.h"
  use petscts
  use Option_module
  use Output_Aux_module
  use Realization_Base_class
  use Solver_module

  use PFLOTRAN_Constants_module

  implicit none

  private

  type, public :: pm_base_type
    character(len=MAXWORDLENGTH) :: name
    type(option_type), pointer :: option
    type(output_option_type), pointer :: output_option
    Vec :: solution_vec
    Vec :: residual_vec
    PetscBool :: print_ekg
    !TODO(geh): remove solver and place required convergence settings elsewhere
    type(solver_type), pointer :: solver
    class(realization_base_type), pointer :: realization_base
    class(pm_base_type), pointer :: next
    !! for hybrid method:
    Vec :: hybrid_fixed_residual_vec
  contains
    procedure, public :: Setup => PMBaseSetup
    procedure, public :: Read => PMBaseRead
    procedure, public :: InitializeRun => PMBaseThisOnly
    procedure, public :: InputRecord => PMBaseInputRecord
    procedure, public :: SetSolver => PMBaseSetSolver
    procedure, public :: FinalizeRun => PMBaseThisOnly
    procedure, public :: Residual => PMBaseResidual
    procedure, public :: Jacobian => PMBaseJacobian
    procedure, public :: UpdateTimestep => PMBaseUpdateTimestep
    procedure, public :: InitializeTimestep => PMBaseThisOnly
    procedure, public :: PreSolve => PMBaseThisOnly
    procedure, public :: Solve => PMBaseThisTimeError
    procedure, public :: PostSolve => PMBaseThisOnly
    procedure, public :: FinalizeTimestep => PMBaseThisOnly
    procedure, public :: AcceptSolution => PMBaseFunctionThisOnly
    procedure, public :: CheckUpdatePre => PMBaseCheckUpdatePre
    procedure, public :: CheckUpdatePost => PMBaseCheckUpdatePost
    procedure, public :: CheckConvergence => PMBaseCheckConvergence
    procedure, public :: TimeCut => PMBaseThisOnly
    procedure, public :: UpdateSolution => PMBaseThisOnly
    procedure, public :: UpdateAuxVars => PMBaseThisOnly
    procedure, public :: MaxChange => PMBaseThisOnly
    procedure, public :: ComputeMassBalance => PMBaseComputeMassBalance
    procedure, public :: Destroy => PMBaseThisOnly
    procedure, public :: RHSFunction => PMBaseRHSFunction
    procedure, public :: CheckpointBinary => PMBaseCheckpointBinary
    procedure, public :: RestartBinary => PMBaseCheckpointBinary
    procedure, public :: CheckpointHDF5 => PMBaseCheckpointHDF5
    procedure, public :: RestartHDF5 => PMBaseCheckpointHDF5
    !! for EI methods:
    procedure, public :: UpdateTimestepNonNewton => PMBaseUpdateTimestepNonNewton
    procedure, public :: ResidualRHS => PMBaseResidual
    procedure, public :: JacobianRHS => PMBaseJacobian
    procedure, public :: ChangeSubjectForEI => PMBaseChangeSubjectForEI
    !! for hybrid EI-iterative solver methods
    procedure, public :: HybridResidual => PMBaseHybridResidual
    procedure, public :: HybridJacobian => PMBaseHybridJacobian
    procedure, public :: AccumNoDT => PMBaseAccumNoDT
    procedure, public :: AccumNoDTDeriv => PMBaseAccumNoDTDeriv
    procedure, public :: AccumNoDTDerivInv => PMBaseAccumNoDTDerivInv
    procedure, public :: AcceptHybridFixedResidualVec => PMBaseAcceptHFRV
    procedure, public :: AcceptHybridFixedResidualArray => PMBaseAcceptHFRV_array
  end type pm_base_type
  
  type, public :: pm_base_header_type
    PetscInt :: ndof
  end type pm_base_header_type
    
  public :: PMBaseInit, &
            PMBaseInputRecord, &
            PMBaseResidual, &
            PMBaseJacobian, &
            PMBaseRHSFunction
  
contains

! ************************************************************************** !

subroutine PMBaseInit(this)

  implicit none
  
  class(pm_base_type) :: this  

  ! Cannot allocate here.  Allocation takes place in daughter class
  this%name = ''
  nullify(this%option)
  nullify(this%output_option)
  nullify(this%realization_base)
  nullify(this%solver)
  this%solution_vec = PETSC_NULL_VEC
  this%residual_vec = PETSC_NULL_VEC
  this%hybrid_fixed_residual_vec = PETSC_NULL_VEC
  this%print_ekg = PETSC_FALSE
  nullify(this%next)
  
end subroutine PMBaseInit

! ************************************************************************** !

subroutine PMBaseRead(this,input)
  use Input_Aux_module
  implicit none
  class(pm_base_type) :: this
  type(input_type), pointer :: input
  print *, 'Must extend PMBaseRead for: ' // trim(this%name)
  stop
end subroutine PMBaseRead

! ************************************************************************** !

subroutine PMBaseSetup(this)
  implicit none
  class(pm_base_type) :: this
  print *, 'Must extend PMBaseSetup for: ' // trim(this%name)
  stop
end subroutine PMBaseSetup

! ************************************************************************** !

subroutine PMBaseInputRecord(this)
  implicit none
  class(pm_base_type) :: this
  print *, 'Must extend PMBaseInputRecord for: ' // trim(this%name)
  stop
end subroutine PMBaseInputRecord

! ************************************************************************** !

subroutine PMBaseResidual(this,snes,xx,r,ierr)
  implicit none
  class(pm_base_type) :: this
  SNES :: snes
  Vec :: xx
  Vec :: r
  PetscErrorCode :: ierr
  print *, 'Must extend PMBaseResidual for: ' // trim(this%name)
  stop
end subroutine PMBaseResidual

! ************************************************************************** !

subroutine PMBaseJacobian(this,snes,xx,A,B,ierr)
  implicit none
  class(pm_base_type) :: this
  SNES :: snes
  Vec :: xx
  Mat :: A, B
  PetscErrorCode :: ierr
  print *, 'Must extend PMBaseJacobian for: ' // trim(this%name)
  stop
end subroutine PMBaseJacobian

! ************************************************************************** !

subroutine PMBaseUpdateTimestep(this,dt,dt_min,dt_max,iacceleration, &
                                num_newton_iterations,tfac, &
                                time_step_max_growth_factor)
  implicit none
  class(pm_base_type) :: this
  PetscReal :: dt
  PetscReal :: dt_min,dt_max
  PetscInt :: iacceleration
  PetscInt :: num_newton_iterations
  PetscReal :: tfac(:)
  PetscReal :: time_step_max_growth_factor
  print *, 'Must extend PMBaseUpdateTimestep for: ' // trim(this%name)
  stop
end subroutine PMBaseUpdateTimestep

! ************************************************************************** !

subroutine PMBaseCheckUpdatePre(this,line_search,X,dX,changed,ierr)
  implicit none
  class(pm_base_type) :: this
  SNESLineSearch :: line_search
  Vec :: X
  Vec :: dX
  PetscBool :: changed
  PetscErrorCode :: ierr
  print *, 'Must extend PMBaseCheckUpdatePre for: ' // trim(this%name)
  stop
end subroutine PMBaseCheckUpdatePre

! ************************************************************************** !

subroutine PMBaseCheckUpdatePost(this,line_search,X0,dX,X1,dX_changed, &
                                 X1_changed,ierr)
  implicit none
  class(pm_base_type) :: this
  SNESLineSearch :: line_search
  Vec :: X0
  Vec :: dX
  Vec :: X1
  PetscBool :: dX_changed
  PetscBool :: X1_changed
  PetscErrorCode :: ierr
  print *, 'Must extend PMBaseCheckUpdatePost for: ' // trim(this%name)
  stop
end subroutine PMBaseCheckUpdatePost

! ************************************************************************** !

subroutine PMBaseCheckConvergence(this,snes,it,xnorm,unorm,fnorm,reason,ierr)
  implicit none
  class(pm_base_type) :: this
  SNES :: snes
  PetscInt :: it
  PetscReal :: xnorm
  PetscReal :: unorm
  PetscReal :: fnorm
  SNESConvergedReason :: reason
  PetscErrorCode :: ierr
  print *, 'Must extend PMBaseCheckConvergence for: ' // trim(this%name)
  stop
end subroutine PMBaseCheckConvergence

! ************************************************************************** !

subroutine PMBaseThisOnly(this)
  implicit none
  class(pm_base_type) :: this
  print *, 'Must extend PMBaseThisOnly for: ' // trim(this%name)
  stop
end subroutine PMBaseThisOnly

! ************************************************************************** !

subroutine PMBaseThisTime(this,time)
  implicit none
  class(pm_base_type) :: this
  PetscReal :: time
  print *, 'Must extend PMBaseThisTime for: ' // trim(this%name)
  stop
end subroutine PMBaseThisTime

! ************************************************************************** !

subroutine PMBaseThisTimeError(this,time,ierr)
  implicit none
  class(pm_base_type) :: this
  PetscReal :: time
  PetscErrorCode :: ierr
  print *, 'Must extend PMBaseThisTimeError for: ' // trim(this%name)
  stop
end subroutine PMBaseThisTimeError

! ************************************************************************** !

function PMBaseFunctionThisOnly(this)
  implicit none
  class(pm_base_type) :: this
  PetscBool ::  PMBaseFunctionThisOnly
  PMBaseFunctionThisOnly = PETSC_TRUE
  print *, 'Must extend PMBaseFunctionThisOnly for: ' // trim(this%name)
  stop
end function PMBaseFunctionThisOnly

! ************************************************************************** !

subroutine PMBaseComputeMassBalance(this,mass_balance_array)
  implicit none
  class(pm_base_type) :: this
  PetscReal :: mass_balance_array(:)
  print *, 'Must extend PMBaseComputeMassBalance for: ' // trim(this%name)
  stop
end subroutine PMBaseComputeMassBalance


! ************************************************************************** !

subroutine PMBaseSetSolver(this,solver)
  ! 
  ! Author: Glenn Hammond
  ! Date: 11/15/17

  use Solver_module

  implicit none

  class(pm_base_type) :: this
  type(solver_type), pointer :: solver

  this%solver => solver

end subroutine PMBaseSetSolver

! ************************************************************************** !

subroutine PMBaseRHSFunction(this,ts,time,xx,ff,ierr)
  implicit none
  class(pm_base_type) :: this
  TS :: ts
  PetscReal :: time
  Vec :: xx
  Vec :: ff
  PetscErrorCode :: ierr
  print *, 'Must extend PMBaseRHSFunction for: ' // trim(this%name)
  stop
end subroutine PMBaseRHSFunction

! ************************************************************************** !

subroutine PMBaseCheckpointBinary(this,viewer)
  implicit none
#include "petsc/finclude/petscviewer.h"      
  class(pm_base_type) :: this
  PetscViewer :: viewer
!  print *, 'Must extend PMBaseCheckpointBinary/RestartBinary.'
!  stop
end subroutine PMBaseCheckpointBinary

! ************************************************************************** !

subroutine PMBaseCheckpointHDF5(this, pm_grp_id)

#if  !defined(PETSC_HAVE_HDF5)
  implicit none
  class(pm_base_type) :: this
  integer :: pm_grp_id
  print *, 'PFLOTRAN must be compiled with HDF5 to ' // &
        'write HDF5 formatted checkpoint file. Darn.'
  stop
#else

  use hdf5
  implicit none

  class(pm_base_type) :: this
#if defined(SCORPIO_WRITE)
  integer :: pm_grp_id
#else
  integer(HID_T) :: pm_grp_id
#endif
!  print *, 'Must extend PMBaseCheckpointHDF5/RestartHDF5.'
!  stop
#endif

end subroutine PMBaseCheckpointHDF5

! *****************************************************
! Below are routines added for exponential integrators
! - D Stone

subroutine PMBaseUpdateTimestepNonNewton(this,dt,dt_min,dt_max, errest, option, &
                                         iacceleration)
  implicit none
  class(pm_base_type) :: this
  class(option_type) :: option
  PetscReal :: dt
  PetscReal :: dt_min,dt_max, errest
  PetscInt :: iacceleration
  print *, 'Must extend PMBaseUpdateTimestepNonNewton for: ' // trim(this%name)
  stop
end subroutine PMBaseUpdateTimestepNonNewton

subroutine PMBaseChangeSubjectForEI(this,oldRHS, newRHS, oldJ, newJ, util1, util2, util3, workerMat1, workerMat2, &
                                    workerBlockMat1, workerBlockMat2, do_debug, ierr)
  implicit none
  class(pm_base_type) :: this
  Vec :: oldRHS, newRHS
  Mat :: oldJ, newJ
  !Mat :: util1, util2, util3
  Mat :: workerMat1, workerMat2, workerBlockMat1, workerBlockMat2
  Vec :: util1, util2, util3
  PetscErrorCode :: ierr
  PetscBool :: do_debug
  print *, 'Must extend PMBaseChangeSubjectForEI for: ' // trim(this%name)
  stop
end subroutine PMBaseChangeSubjectForEI

! *****************************************************

subroutine PMBaseHybridResidual(this,snes,xx,r,ierr)
  !! hybrid method (EI + SNES operator splitting)
  !! will need a new type of residual.
  implicit none
  class(pm_base_type) :: this
  SNES :: snes
  Vec :: xx
  Vec :: r
  PetscErrorCode :: ierr

  print *, 'Must extend PMBaseHybridResidual for: ' // trim(this%name)
  stop
end subroutine PMBaseHybridResidual

! ************************************************************************** !

subroutine PMBaseHybridJacobian(this,snes,xx,A,B,ierr)
  ! 


  implicit none
  
  class(pm_base_type) :: this
  SNES :: snes
  Vec :: xx
  Mat :: A, B
  PetscErrorCode :: ierr
  
  print *, 'Must extend PMBaseHybridResidual for: ' // trim(this%name)
  stop

end subroutine PMBaseHybridJacobian

! ************************************************************************** !

subroutine PMBaseAccumNoDT(this,a, ierr)
  !! hybrid method (EI + SNES operator splitting)
  !! will require evaluation of the part 
  !! of the governing ODES, u, which is nothing 
  !! but the nonfixed accumlation part
  !! but WITHOUT any factors of dt.

  implicit none
  class(pm_base_type) :: this
  Vec :: a
  PetscErrorCode :: ierr
  print *, 'Must extend PMBaseAccumDt for: ' // trim(this%name)
  stop
end subroutine PMBaseAccumNoDT

subroutine PMBaseAccumNoDTDeriv(this, ad, ierr)
  !! hybrid method (EI + SNES operator splitting)
  !! will require evaluation of the part 
  !! of the governing ODES, u, and it's first derivatives.

  implicit none
  class(pm_base_type) :: this
  Mat :: ad
  PetscErrorCode :: ierr
  print *, 'Must extend PMBaseAccumDtDeriv for: ' // trim(this%name)
  stop
end subroutine PMBaseAccumNoDTDeriv

subroutine PMBaseAccumNoDTDerivInv(this, adi, ierr)
  !! hybrid method (EI + SNES operator splitting)
  !! will require evaluation of the part 
  !! of the governing ODES, u, and it's first derivatives,
  !! and the inverse.
  !! (it's block diagonal so inverse should be cheap)
  !! Note that this routine is exactly what we want to
  !! pass to SNES as a Jacobian so either 
  !! give this the same calling signature or
  !! create a wrapper for it.

  implicit none
  class(pm_base_type) :: this
  Mat :: adi
  PetscErrorCode :: ierr
  print *, 'Must extend PMBaseAccumDtDerivInv for: ' // trim(this%name)
  stop
end subroutine PMBaseAccumNoDTDerivInv

subroutine PMBaseAcceptHFRV(this, xx, ierr)

  !! NOTE: need to ensure that the fixed_residual_vec vector has been set up
  !! first.

  implicit none
  class(pm_base_type) :: this
  Vec :: xx
  PetscErrorCode :: ierr

  call VecCopy(xx, this%hybrid_fixed_residual_vec, ierr); CHKERRQ(ierr)
  !this%hybrid_fixed_residual_vec = xx !! ths way might do duplication as well

end subroutine PMBaseAcceptHFRV

subroutine PMBaseAcceptHFRV_array(this, xx, N, ierr)

  !! accept xx as the hybrid fixed residual
  !! vec for hybrid EI methods,
  !! where xx is an array and not a PETSC vector.

  !! NOTE: need to ensure that the fixed_residual_vec vector has been set up
  !! first.

  use arrayMatrixAlgsModule
  implicit none
  class(pm_base_type) :: this
  PetscInt :: N
  PetscReal, dimension(1:N) :: xx
  PetscErrorCode :: ierr

  !call VecCopy(xx, this%hybrid_fixed_residual_vec, ierr); CHKERRQ(ierr)
  !this%hybrid_fixed_residual_vec = xx !! ths way might do duplication as well
  call ArrayCopyVec(this%hybrid_fixed_residual_vec, xx, N, ierr)

end subroutine PMBaseAcceptHFRV_array

end module PM_Base_class
