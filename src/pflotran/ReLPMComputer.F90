module ReLPMComputerModule

use petscsys
use petscmat
use arrayMatrixAlgsModule

  implicit none

#include "petsc/finclude/petscsys.h"
#include "petsc/finclude/petscmat.h"
#include "petsc/finclude/petscvec.h"
#include "petsc/finclude/petscviewer.h"

  private

  type, public :: relpm_computer

  PetscBool :: initialized
  Vec :: w, wp1
  Mat :: workerMat

  Integer :: nPts
  PetscReal, dimension(:), allocatable :: lejaPts
  PetscReal, dimension(:), allocatable :: lPts

  PetscReal, dimension(:), allocatable :: workerVecArr
    
  contains
    
    procedure, public :: Initialize => relpmCompInitialize
    procedure, public :: Compute => ReLPM_compute 

  end type relpm_computer

public :: relpmCompCreate

contains

!!***************************************************

function relpmCompCreate()

  implicit none
  
  class(relpm_computer), pointer :: relpmCompCreate 
  
  allocate(relpmCompCreate)

  allocate(relpmCompCreate%lPts (relpmCompCreate%nPts))

  relpmCompCreate%initialized = PETSC_FALSE
  
  
end function relpmCompCreate 

!!***************************************************

subroutine relpmCompInitialize(this, X, v, ierr)

  implicit none
  
  class(relpm_computer) :: this 
  PetscErrorCode :: ierr
  Mat :: X
  Vec :: v

  call MatConvert(X, MATSAME, MAT_INITIAL_MATRIX, this%workerMat, ierr); CHKERRQ(ierr) 
  call vecDuplicate(v, this%w, ierr); CHKERRQ(ierr)
  call vecDuplicate(v, this%wp1, ierr); CHKERRQ(ierr)

  this%nPts = 500
  !allocate(this%workerVecArr (this%nPts))
  allocate(this%lejaPts (this%nPts)) !! this may be problematic with indexing
  !allocate(this%lejaPts (0:this%nPts))

  !! set npts to 200 for now
  !allocate(this%lPts (this%nPts)) !! problem
  !allocate(this%lPts (this%nPts))
  
  
  this%initialized = PETSC_TRUE
  
  
end subroutine relpmCompInitialize 

!!***************************************************

subroutine gersh(L, N, a, b)

  implicit none
  Mat :: L
  Integer :: N
  PetscReal :: a, b
  PetscReal :: dval, offDiagSum, candp, candm
  PetscInt :: i, j, col
  PetscScalar :: vals(N)
  PetscInt :: cols(N)
  PetscInt :: ncols
  PetscErrorCode :: ierr

  call quickMatBinOut(L, "gersh input", ierr)

  do i = 0,N-1
    dval = 0.d0
    offDiagSum = 0.d0

    !! extract row
    call MatGetRow(L, i, ncols, cols, vals, ierr); CHKERRQ(ierr)
    !call MatGetRow(L, i, ncols, cols, PETSC_NULL_SCALAR, ierr); CHKERRQ(ierr)

    do j = 0,ncols-1 !! not clear if this should be 0 indexed or not
      col = cols(j)
      if (col .eq. j) then !! if cols not 0 indexed, need an offset here
        dval = vals(j)
      else
        offDiagSum = offDiagSum + abs(vals(j))
      endif
    end do

    call MatRestoreRow(L, i, ncols, cols, vals, ierr); CHKERRQ(ierr)

    candp = dval + offDiagSum
    candm = dval - offDiagSum

    if (candp > b) then
      b = candp
    endif
    if (candm < a) then
      a = candm
    endif
  end do

end subroutine gersh

subroutine ReLPM_compute(this, N, X, v, p, ierr)


  use lejaModule

  implicit none
  class(relpm_computer) :: this
  Mat :: X
  Vec :: v, p
  !Vec :: w, wp1, p
  Integer ::  m, i, N
  !PetscReal, dimension(0:this%nPts-1) :: d !! can't do this! npts is set on
                                            !! first call, so npts will be not
                                            !! be set by here
  PetscReal, dimension(:), allocatable :: d
  PetscReal :: errest, a, b, TOL, vec_norm
  !PetscInt :: i
  PetscErrorCode :: ierr

  PetscReal :: cheatFactor


  !cheatFactor = 0.001
  !call MatScale(X, cheatFactor, ierr);CHKERRQ(ierr)

  if (.NOT. this%initialized) then
    call this%Initialize(X, v, ierr)
  endif

  allocate(d (0:this%nPts-1))



  TOL = 1.d-15 !! good enough for testing?


  !! Gershgorin Estimate
  call gersh(X, N, a, b)

  !! Compute Leja Points
  call fastLejaPoints(a, b, this%nPts, this%lejaPts)


  !d(0) = phiscal(this%lejaPts(0))
  d(0) = phiscal(this%lejaPts(1))

  !w = v
  call VecCopy(v, this%w, ierr); CHKERRQ(ierr)
  ! / w = v

  !p = d(0)*this%w :
  call VecCopy(this%w, p, ierr); CHKERRQ(ierr)
  call VecScale(p, d(0), ierr ); CHKERRQ(ierr) 
  ! / p = d(0)*this%w

  m = 0

  !! NEED TO ADJUST FOR FACT THAT LPTS ARRAY
  !! WAS MADE 1-INDEXED
  !! DONE

  !! THIS DOESN'T SEEM TO CONVERGE

  !errest = abs(d(m))*norm(w):
  call VecNorm(this%w, NORM_2, vec_norm, ierr); CHKERRQ(ierr)
  errest = abs(d(m))*vec_norm
  ! / errest = abs(d(m))*norm(w)
  do while (errest > TOL .AND. m < this%nPts-2)

    !wp1 = (X - lPts(m)*I)*w:
    !call MatCopy(X, this%workerMat, SAME_NONZERO_PATTERN, ierr); CHKERRQ(ierr)
    !call MatShift(  this%workerMat, -1.d0*this%lejaPts(m), ierr); CHKERRQ(ierr)
    !call MatMult(this%workerMat, this%w, this%wp1, ierr); CHKERRQ(ierr)
    ! / wp1 = (X - lPts(m)*I)*w
    !wp1 = (X - lPts(m)*I)*w:
    ! 1) X*w:
    call MatMult(X, this%w,  this%wp1, ierr);CHKERRQ(ierr)
    ! 2) -lPts(m)*w
    !call VecAXPY(this%wp1, -1.d0*this%lejaPts(m), this%w, ierr);CHKERRQ(ierr)
    call VecAXPY(this%wp1, -1.d0*this%lejaPts(m+1), this%w, ierr);CHKERRQ(ierr)
    ! / wp1 = (X - lPts(m)*I)*w

    ! then w <- wp1
    call VecCopy(this%wp1, this%w, ierr);CHKERRQ(ierr)

    m = m + 1
    !d(m) = phiscal(this%lejaPts(m))
    d(m) = phiscal(this%lejaPts(m+1))
    !print *, "d before ddiffs: ", d(m)

    do i = 1,m
      !d(m) = (d(m) - d(i-1))/(this%lejaPts(m) - this%lejaPts(i-1))
      d(m) = (d(m) - d(i-1))/(this%lejaPts(m+1) - this%lejaPts(i))
    end do
    !print *, "d: ", d(m)

    ! p = p + d(m)*w:
    call VecAXPY(p, d(m), this%w, ierr); CHKERRQ(ierr)
    ! / p = p + d(m)*w

    !errest = abs(d(m))*norm(w):
    call VecNorm(this%w, NORM_2, vec_norm, ierr); CHKERRQ(ierr)
    errest = abs(d(m))*vec_norm
    ! / errest = abs(d(m))*norm(w)
    !print *, "errest : ", errest


    !print *, "est error ", errest

  end do
  print *, "leja interpolation ended with error ", errest, " and m ", m


   deallocate(d)

end subroutine ReLPM_compute


function phiscal(z)
  
  implicit none
  PetscReal :: phiscal
  PetscReal :: z
  PetscReal :: az

  az = abs(z)
  

  !print *, "phiscal input: ",z

  if (az < 1.d-16) then
    phiscal = 1.0d0
  elseif (az < 1.d0) then
    phiscal = (exp(z) - 1)/log(exp(z))
  else
    phiscal = (exp(z) - 1)/z
  endif
  !print *, "phifunc output ",phiscal 

end function phiscal

end module ReLPMComputerModule
