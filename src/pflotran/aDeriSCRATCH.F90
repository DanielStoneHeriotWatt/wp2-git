
!! this is the result of richards flux derivatives:
!! it is the flux in the mass ballance eq, dif w.r.t pressure
!! not the entire function provides two values for each face, up and
!! downstream.
!! For TOIL, provide two 3x3 mats - one up and downstream
!! Once again, requires some severe tabulation of derivatives, but
!! straightfoward
      v_darcy= Dq * ukvr * dphi
   
      q = v_darcy * area
      dq_dp_up = Dq*(dukvr_dp_up*dphi+ukvr*dphi_dp_up)*area
      dq_dp_dn = Dq*(dukvr_dp_dn*dphi+ukvr*dphi_dp_dn)*area
      
      Jup(1,1) = (dq_dp_up*density_ave+q*dden_ave_dp_up)
      Jdn(1,1) = (dq_dp_dn*density_ave+q*dden_ave_dp_dn)
