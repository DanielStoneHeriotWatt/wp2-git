module lejaModule

contains 

subroutine hello()
  implicit none
  print *, "hello from leja module"
end subroutine hello

function prodDiff(x, yarr, leny)
  !! calculates the product
  !! (x -  y[1])(x- y[2])...
  
  implicit none
  real*8 :: prodDiff
  real*8 :: x
  integer :: leny 
  real*8, dimension(1:leny) :: yarr
  integer :: i
  real*8 :: newfac

  prodDiff = x - yarr(1)

  do i = 2,leny
    prodDiff = prodDiff*(x-yarr(i))
    !newfac = (x - yarr)
  end do

end function prodDiff

subroutine maxAndValAbs(xarr, lenx, val, loc)

  implicit none
  integer :: lenx
  real*8, dimension(1:lenx) :: xarr
  real*8 :: val
  real*8 :: valcand
  integer :: loc
  integer :: i

  loc = 1
  val = abs(xarr(1))

  do i = 1,lenx
    valcand = abs(xarr(i))
    if (valcand > val) then
      val = valcand
      loc = i
    endif
  end do

end subroutine maxAndValAbs

!subroutine fastLejaPoints(a, b, nflp, zs)
subroutine fastLejaPoints(a, b, nflp, zt)
 !! port of fast leja points algorithm by 
 !! [fastLeja]


  implicit none
  real*8 :: a, b !! left and right pts of interval
  integer :: nflp !! number of points to calculate
  real*8, dimension(1:nflp) :: zs 
  real*8, dimension(1:nflp) :: zt 
  real*8, dimension(1:nflp) :: zprod !! product zs - zt_k
  integer, dimension(1:nflp, 2) :: indexs !! (k,1) -> pointer
                                          !! to LP left of zs(k)
                                          !! (k,2) -> to right
  integer :: i, j, zprod_size, zt_size, maxi,   dex1, dex2
  real*8 :: mxvl

  zprod = 0.d0

  !! first points are endpoints..
  if (abs(a) > abs(b)) then
    zt(1) = a
    zt(2) = b
  else
    zt(1) = b
    zt(2) = a
  endif
  !! ... and midpoint
  zt(3) = (a+b)/2.d0
  zt_size = 3

  !! first two candidate points:
  zs(1) = (zt(2) + zt(3))/2.d0
  zs(2) = (zt(3) + zt(1))/2.d0

  !! update zprod, currently over 3
  !! candidate points
  zprod(1) = prodDiff(zs(1), zt(1:3), 3)
  zprod(2) = prodDiff(zs(2), zt(1:3), 3)

  !! ..
  !zprod_size = 2
  zprod_size = nflp


  indexs(1,1) = 2 !! i.e., the point to the left of zs(1)
                  !! is zt(1)
  indexs(1,2) = 3
  indexs(2,1) = 3
  indexs(2,2) = 1


  do i = 4,nflp
    !! inefficiency here: really only need max returned
    !! could use maxloc builtin, but still need to abs everything
    call maxAndValAbs(zprod(1:zprod_size), zprod_size, mxvl, maxi)
    zt(i) = zs(maxi) !! new point is one which maximizes prod, which
                     !! is the definition.
    zt_size = zt_size + 1
    !! update indexes:
    indexs(i-1,1) = i
    indexs(i-1,2) = indexs(maxi, 2)
    indexs(maxi,2) = i

    !! replace candidate at maxi:
    dex1 = indexs(maxi, 1)
    dex2 = indexs(maxi, 2)

    zs(maxi) = ( zt(indexs(maxi,1)) +  zt(indexs(maxi,2)) )/2.d0
    !! note that this                   /\
    !! is the candidate being replaced. What's happening here is
    !! a new candidate is being squeezed in between the old candidate
    !! and its neighbour.

    zs(i-1) = ( zt(indexs(i-1,1)) +  zt(indexs(i-1,2)) )/2.d0
    !! this is whole new candidate 

    !! update products:
    !! because zs(maxi) was changed:
    zprod(maxi) = prodDiff(zs(maxi), zt(1:i-1), i-1) !! note incomplete, see next few
                                                     !! lines
    !! because zs(i-1) was added:
    zprod(i-1) = prodDiff(zs(i-1), zt(1:i-1), i-1) !! see next line
    !! because zt(i) was added:
    do j = 1,i-1 !! multiply all prods by new diff for zt(i)
      zprod(j) = zprod(j)*(zs(j) - zt(i))
    end do

  end do

    

end subroutine fastLejaPoints



!! bibliography:
!! [fastLeja] : Baglama, Calvetti, Reichel,
!!              "Fast Leja Points" 
!!              Elec. Trans. Numer. Anal. 7
!!              (1998), 124-140

end module lejaModule
