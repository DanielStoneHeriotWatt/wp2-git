\documentclass{article}
\usepackage{amsmath}



\begin{document}
\begin{center}
{\huge \bf Exponential Integrators in Pflotran} \\
Daniel Stone
\end{center}
\section{The basic exponential integrator and required matrix and vector}
Assume the governing equation is spatially semi-discretised (e.g. by finite volume) into an ODE system:
\begin{equation}
\frac{d \mathbf{u}}{dt}  =\mathbf{g}(u)
\label{ode_sys}
\end{equation}
Let $J_g$ be the Jacobian matrix of the RHS $\mathbf{g}(u)$ in (\ref{ode_sys}):
\begin{equation}
J_g \equiv   \frac{\partial \mathbf{g}(u)}{\partial \mathbf{u}}
\label{jac_g}
\end{equation}
Then the simplest exponential integrator that can be applied to (\ref{ode_sys}) is EEM:
\begin{equation}
\mathbf{u}_{n+1} = \mathbf{u}_{n} + \Delta t \varphi_1(\Delta t J_g) \mathbf{g}(\mathbf{u}_n),
\label{eem}
\end{equation}
where $\varphi_1(z)$ is a matrix exponential function. Scheme (\ref{eem}) is an explicit, single step, second order timestepper that (in principle) admits large timesteps and good stability properties. Prototype routines for evaluating $\varphi_1(z)$ have now been written for Petsc matrices that should be compatible with Pflotran. \\
Note that EEM (\ref{eem}) is not the basic exponential integrator ETD1 that was shown in my introductory talk; ETD1 as well as most exponential integrators require a semilinear form of (\ref{ode_sys}) which isn't possible for most systems of interest here.

\section{Iterative solver form and the matrix and vector available in Pflotran}
Let $\mathbf{v}$ be the (vector of) the variables to be solved for. This may or may not be $\mathbf{u}$. For example, in the Richardson formulation, we have $\mathbf{u} = \phi \rho s$ while $\mathbf{v} = \mathbf{p}$, the pressure. In multiphase, the variables in $\mathbf{v}$ change dynamically to follow state changes (variable switching).
Equations of state are available for converting between $\mathbf{u}$ and $\mathbf{v}$ in pflotran, for example the Van Genuchten equation gives $s$ in terms of $p$, and so on. \\
In light of this it is clearer to write (\ref{ode_sys}) in terms of $\mathbf{v}$:
\begin{equation}
\frac{d \mathbf{u}(\mathbf{v})}{dt}  =  \mathbf{g}(\mathbf{u}(\mathbf{v})).
\label{ode_sys_v}
\end{equation}
The iterative solve formulation on which pflotran is built allows $\mathbf{v}$ to be chosen somewhat arbitrarily (c.f. variable switching), and in particular we do not have to solve for the variable in the time derivative, $\mathbf{u}$. Unfortunately, exponential integrators \emph{do} require that we solve for $\mathbf{u}$. Accounting for this constitutes the bulk of the work done and still to do so far for implementing exponential integrators in pflotran. \\
Let us state the basic process of the iterative solver approach. The standard approach to solve (\ref{ode_sys_v}) is to use an iterative solver to find the root of:
\begin{equation}
\mathbf{R}(\mathbf{v}) \equiv \frac{d \mathbf{u}(\mathbf{v})}{dt}  -\mathbf{g}(\mathbf{u}(\mathbf{v})) =0,
\label{ode_Rpure}
\end{equation}
where we refer to $\mathbf{R}$ as the residual. The Jacobian $J_R$ of the residual is also required:
\begin{equation}
J_R \equiv   \frac{\partial \mathbf{R}(\mathbf{v})}{\partial \mathbf{\mathbf{v}}}.
\label{jac_R}
\end{equation}
The time derivative in (\ref{ode_Rpure}) must be approximated, e.g. by Backwards Euler:
\begin{equation}
\mathbf{R} \approx \frac{ \mathbf{u}_{n} - \mathbf{u}_{n-1}  }{\Delta t}  -\mathbf{g}_n =0,
\label{ode_R1}
\end{equation}
or alternatively sometimes people like to multiply through by $\Delta t$ and solve for a slightly different residual with the same root:
\begin{equation}
\mathbf{R} \approx  \mathbf{u}_{n} - \mathbf{u}_{n-1}  - \Delta t \mathbf{g}_n =0
\label{ode_R2}
\end{equation}
or possibly other variations. I can't find it written down explicitly which is used but by looking we can find which is used. In Richardson for example we know that it is (\ref{ode_R1}) .  \\
Pflotran of course produces $\mathbf{R}(\mathbf{v})$ and $J_R$; the task is to take these and use them to obtain the required $\mathbf{g}(\mathbf{u})$ and $J_g$.

\section{How to get $g(u)$ and $J_g$}
\subsection{Direct relations}
From (\ref{ode_R1}), it is easy enough to relate $\mathbf{R}$ and $\mathbf{g}$:
\begin{equation}
\mathbf{g}(\mathbf{u}_n) =  \frac{ \mathbf{u}_{n} - \mathbf{u}_{n-1}  }{\Delta t} - \mathbf{R}(\mathbf{v}_n).
\label{rtog}
\end{equation}
We can either implement this computation, or implement a modified version of the routine to compute $R$. In some cases the code has been written in a modular way that
makes this simple, for example in Richardson, the addition of the part $\frac{ \mathbf{u}_{n} - \mathbf{u}_{n-1}  }{\Delta t}$ is handled by a call to the routine RichardsCalculateAccumulation(),
so a version of the subroutine to compute the $\mathbf{R}$ with that call removed and the final result multiplied by $-1$ gives $\mathbf{g}$. \\
Getting $J_g$ from $J_R$ is trickier. Note that in light of (\ref{rtog}) we can easily obtain:
\begin{equation}
\frac{\partial \mathbf{g}}{\partial \mathbf{v}} =  I - J_R,
\label{jrtojg}
\end{equation}
Now if $\mathbf{u} = \mathbf{v}$ then we would have $\frac{\partial \mathbf{g}}{\partial \mathbf{v}} = J_g$. Since this is not the case we have to try something more complicated.

\subsection{Approaches}
The first potential approach to this problem would be to directly implement code for $J_g$, and then implement timesteppers, solvers and process models to solve (\ref{ode_sys}) in terms of $\mathbf{u}$. This can be done but by that point we may as well be writing a completely new package. Also refusing to ever allow the solution variable to change will likely cause significant difficulties with multiphase, where dynamic variable switching is the standard method.  \\
The second approach is to attempt to use $J_R$ and $R$ to create an ODE in terms of $\mathbf{v}$, which the exponential integrator can then work with. Understanding how to do this type of changing the subject of the ODE will likely be the foundation of how we end up tackling variable switching in multiphase.

\subsection{An ODE in terms of $v$}
We want an ODE of the form:
$$
\frac{d \mathbf{v}}{dt} = \mathbf{h}(\mathbf{v})
$$
and a corresponding Jacobian:
$$
\frac{\partial \mathbf{h}}{\partial \mathbf{v}}
$$
so that we can use an exponential integrator to evolve $\mathbf{v}$. We also assume we have access to a function for $\mathbf{v}$ in terms of $\mathbf{u}$:
$$
\mathbf{v} = \mathbf{z}(\mathbf{u}).
$$
Further assume that each individual component $v_i$ of $\mathbf{v}$ depends only on the corresponding component of $\mathbf{u}$:
\begin{equation}
v_i = z(u_i),
\label{single_celled}
\end{equation}
that is, the value of $\mathbf{v}$ in any cell depends on the value of $\mathbf{u}$ only in that cell. This is exactly how pflotran handles axillary variable calculations, for example, pressure and saturation could be related by the VG equation,
but the relation is single cell only. \\
We also have $\mathbf{g}$ from (\ref{ode_sys}) and (\ref{rtog}) available, as well as $\frac{\partial \mathbf{g}}{\partial \mathbf{v}}$ from (\ref{jrtojg}). \\
Using the chain rule we seem to have an easy way to get $h$:
$$
h_i = \frac{d v_i}{d t} = \sum_j{ \frac{\partial v_i}{\partial u_j} \frac{d u_j}{dt} } =  \frac{\partial v_i}{\partial u_i} \frac{\partial u_i}{\partial t} = \frac{\partial v_i}{\partial u_i} g_i,
$$
where (\ref{single_celled}) has caused all of the terms in the sum to be $0$ except $\frac{\partial v_i}{\partial u_i}$. We also have that $\frac{\partial v_i}{\partial u_i} = \frac{d z(u_i)}{d u}$ since $z$ is a scalar function. \\
We thus need to include routines to compute $\frac{d z(u)}{d u}$ in pflotran. Typically this can be done in terms of existing derivatives that are already implemented, for example those that exist in order to implement analytic derivatives
for Richardson. \\
The Jacobian $\frac{\partial \mathbf{h}}{\partial \mathbf{v}}$ is more complicated. Consider the $i,j$ entry:
\begin{equation}
\frac{\partial h_i}{\partial p_j} =  \frac{\partial}{\partial p_j}\left[ \frac{d z(u_i)}{d u} g_i \right] = \frac{\partial}{\partial p_j}\left[ \frac{d z(u_i)}{d u}  \right] g_i + \frac{d z(u_i)}{d u}  \frac{\partial g_i}{\partial p_j},
\label{jac_1}
\end{equation}
where we have applied the standard product rule. To simplify (\ref{jac_1}) first note that $ \frac{\partial g_i}{\partial p_j}$ is the $i,k$th entry of the Jacobian $\frac{\partial \mathbf{g}}{\partial \mathbf{v}}$, so we can happily re-use that. \\
The remaining term to consider is 
$$
\frac{\partial}{\partial p_j}\left[ \frac{d z(u_i)}{d u}  \right]  = \frac{d^2 z(u_i)}{d u d p} \frac{d u_i}{d p} \mbox{ if } i = j \mbox{ else } 0.
$$
For this second derivatives for EOS functions need to be implemented in pflotran. Some have been. \\
Update: this last equation is \emph{wrong}. The correct application of the chain rule is
$$
\frac{d^2 z(u_i)}{d u d u} \frac{d u_i}{d p},
$$
so that the second order derivative we really need is
$$
\frac{d^2 z}{d u^2},
$$
or the good old
$$
\frac{d^2 z}{d u dp}
$$
should be equivalent.


\section{Case Study: Richardson}
For Richardson we have
$$
u = \phi \rho(p) s(p),
$$
porosity, density, saturation, and
$$
v = p,
$$
pressure. Already implemented in pflotran are the derivatives $\frac{d \rho}{dp}$ and $\frac{ds}{dp}$ so to get
$ \frac{dz}{du} = \frac{dp}{du}$ we have implemented:
$$
\frac{du}{dp} = \phi \frac{d \rho}{dp} s + \phi  \rho \frac{ds}{dp},
$$
and thus
$$
\frac{dp}{du} = \frac{1}{\frac{du}{dp} }  = \frac{1}{ \phi \frac{d \rho}{dp} s + \phi  \rho \frac{ds}{dp}}.
$$
This is problematic in certain situations, for example when the system saturates and $\frac{ds}{dp}$ becomes zero. Techniques for these
cases will have to be considered. \\
For the second derivative $\frac{d^2 z}{d u d p}$ we then have
$$
\frac{d^2 z(u_i)}{d u d p} = \frac{d}{dp} \frac{dp}{du}  = -\phi \frac{ \frac{d^2 \rho}{dp^2} s + \rho \frac{d^2s}{dp^2} + 2 \frac{d \rho}{dp}  \frac{ds}{dp}  }{(\phi \frac{d \rho}{dp} s + \phi  \rho \frac{ds}{dp})^2},
$$
for which the second derivatives $\frac{d^2 \rho}{dp^2}$, $\frac{d^2s}{dp^2}$.  had to be implemented.

\subsection{Second derivatives}
\subsubsection{Of $s$}
This is for the VG relation. \\
There is a nice expression of the first derivative of $s$, including the conversion between effective and true saturation, and pressure and cap. pressure, in SF\_VG\_Saturation() (characteristic\_curves.F90). 
$$
\frac{d s}{d p} = -(1 - s_r) \left( -m n \alpha^n p_c^{n-1} [1 + (\alpha p_c)^n]^{-(m+1)} \right).
$$
We can differentiate w.r.t. cap. pressure again:
\begin{equation}
\begin{split}
\frac{d}{d p_c} \left( \frac{d s}{d p}  \right) = & \\
  -(1 - s_r) (-mn \alpha^n) (& (n-1) p_c^{n-2}[1 + (\alpha p_c)^n]^{-(m+1)} \\
&                       +  p_c^{n-1} (-m -1) [1 + (\alpha p_c)^n]^{-(m+2)} n p_c^{n-1} \alpha^n ) \\
= & \\
& \frac{d s}{d p} \left( \frac{n-1}{p_c}  -n (m+1) \frac{p_c^{n-1} \alpha^n}{1 + (\alpha p_c)^n} \right).
\end{split}
\end{equation}
Then, because $\frac{d p_c}{dp} = -1$, 
$$
\frac{d^2s}{dp^2} =  -\frac{d s}{d p} \left( \frac{n-1}{p_c}  -n (m+1) \frac{p_c^{n-1} \alpha^n}{1 + (\alpha p_c)^n} \right).
$$
\subsection{Of $\rho$}
Richards uses for density the interface EOSWaterDensityExt(), which redirects to EOSWaterDensityBatzleAndWang().
The equations used can easily be found in \emph{Seismic properties of pore fluids; Batzle and Wang}, equation 27a:
\begin{equation}
\begin{split}
\rho = & \\
 &1 + 10^{-6} \times ( -80 T - 3.3 T^2  + 0.00175 T^3 \\
 & + 489 P - 2 T P + 0.016 T^2 P - 1.3\times10^{-5}T^3P - 0.333 P^2 - 0.002TP^2 )
\end{split}
\end{equation}
First derivative:
\begin{equation}
\begin{split}
\frac{ \partial \rho}{\partial P} \ = & \\
 & 10^{-6} \times ( 0\\
 & + 489  - 2 T + 0.016 T^2  - 1.3\times10^{-5}T^3 - 0.666 P - 0.004TP )
\end{split}
\end{equation}
Second derivative:
\begin{equation}
\begin{split}
\frac{ \partial^2 \rho}{\partial P^2} \ = & \\
 & 10^{-6} \times ( 0\\
 &  - 0.666 - 0.004T )
\end{split}
\end{equation}
Other implementations include EOSWaterDensityIFC67(), which is much less straightforward. Second derivative may
already be implemented there however.

\section{TOIL}
Three different constitutive equations (consider for now on one cell):
$$
\frac{d v_j}{dt} = h_j \mbox{     } j = 1,2,3
$$
required from
$$
\frac{d u_j}{dt}  = g_j \mbox{     } j = 1,2,3.
$$
The three solution variables are oil pressures, $v_1 = p_o$, oil saturation, $v_2 = s_o$, and temperature, $v_3 = T$. \\
The three functions inside the time derivatives are:
$$
u_1  = \phi s_o \eta_o,
$$
$$
u_2  = \phi s_w \eta_w = \phi (1- s_o) \eta_w,
$$
$$
u_3 = \phi \sum_{\alpha}{s_{\alpha} \eta_{\alpha} U_{\alpha}} + (1- \phi)\rho_r c_r T.
$$
\subsection{$u_1$}
We can make use of:
$$
\frac{\partial u_1(s_o, \eta_o)}{\partial p_o} = \phi s_o  \frac{ \partial \eta_o}{\partial p_o}.
$$
Notes on this: 1) since partial derivative, treat $s_o$ as independent of $p_o$. The derivative $\frac{\partial s_o}{\partial p_o}$ doesn't make sense when both are solution variables. 2) Is the derivative $ \frac{ \partial \eta_o}{\partial p_o}$ manageable? Yes; $\eta _o$ seems to come from the routine \emph{EOSOilDensityEnergy}, which treats $\eta_o = \eta_o{T, p_o}$, and also provides the needed derivative $ \frac{ \partial \eta_o}{\partial p_o}$ anyway. \\
So:
$$
h_1 = \frac{\partial u_1 }{\partial t} \frac{\partial v_1}{\partial u_1} = g_1 \frac{1}{\frac{\partial u_1(s_o, \eta_o)}{\partial p_o}   } = g_1 \frac{1}{   \phi s_o  \frac{ \partial \eta_o}{\partial p_o}  }.
%h_1 = \frac{\partial u_1 }{\partial t} \frac{\partial v_1}{\partial u_1} = g_1 \frac{1}{\frac{\partial u_1(s_o, \eta_o)}{\partial p_o}   } = g_1 \frac{1}{\frac{  \phi s_o  \frac{ \partial \eta_o}{\partial p_o}}
$$
\subsection{$u_2$}
Next make use of:
$$
\frac{\partial u_2(s_o, \eta_w)}{\partial p_o} = \phi s_w \frac{\partial \eta_w}{\partial s_o} - \phi \eta_w.
$$
How much sense does $\frac{\partial \eta_w}{\partial s_o}$ make? Water density seems to be calculated by \emph{EOSWaterDensity}, with dependency $\eta_w = \eta_w(T, p_{cell})$, where $p_{cell}$ is cell pressure. The relationships are the following:
$$
p_{cell} = \max{(p_w,p_o)}
$$
$$
p_w = p_o - p_c
$$
where $p_c$ is capillary pressure. 
$$
p_c = p_c(s_w) = p_c(1-s_o).
$$
So $\frac{\partial p_c}{\partial s_o}$ is in principle obtainable. It may be already included with the output \emph{dpc\_dsatl}. If $p_{cell} = p_o$, then $\eta_w$ is essentially independent of $s_o$. Otherwise:
$$
\frac{\partial \eta_w}{\partial s_o} = \frac{\partial \eta_w}{\partial p_{cell}} \frac{\partial p_{cell}}{\partial s_o}
$$ 
($\frac{\partial \eta_w}{\partial p_{cell}}$, derivative w.r.t. pressure, should be obtainable using the derivatives version of \emph{EOSWaterDensity}. This call is not included in \emph{ComputeAuxVar} at the moment.)
$$
\frac{\partial p_{cell}}{\partial s_o} = - \frac{\partial p_c}{\partial s_o} \mbox{   (or $0$.)}
$$
So
$$
\frac{\partial \eta_w}{\partial s_o} = - \frac{\partial \eta_w}{\partial p_{cell}}  \frac{\partial p_c}{\partial s_o}
$$
So,
$$
\frac{\partial u_2(s_o, \eta_w)}{\partial p_o}  = -  \phi s_w \frac{\partial \eta_w}{\partial p_{cell}}  \frac{\partial p_c}{\partial s_o} - \phi \eta_w,
$$
or just
$$
\frac{\partial u_2(s_o, \eta_w)}{\partial p_o}  = - \phi \eta_w.
$$
So, 
$$
h_2 = \frac{\partial u_2 }{\partial t} \frac{\partial v_2}{\partial u_2} = g_2 \frac{1}{\frac{\partial u_2(s_o, \eta_w)}{\partial s_o}   }
$$
$$
=  g_2 \frac{1}{     -  \phi s_w \frac{\partial \eta_w}{\partial p_{cell}}  \frac{\partial p_c}{\partial s_o} - \phi \eta_w               }
$$
or
$$
g_2 \frac{1}{      - \phi \eta_w               }.
$$
\subsection{$u_3$}
What are $\rho_r$ and $c_r$? Seem not to be explained in TOIL manual. Observe the accumulation subroutine for TOIL to attempt ascertain what they are, and their dependencies. \\
Believe: soil particle density, soil heat capacity. First is stored in material\_auxvar, second seems to be stored as material parameter. Believe these are independent of sol variables. \\
In that case:
$$
\frac{\partial u_3 }{\partial T } = \phi \sum_{\alpha}{s_{\alpha} \left( \frac{\partial \eta_{\alpha}  }{\partial T} U_{\alpha} + \eta_{\alpha}\frac{\partial  U_{\alpha}}{\partial T} \right)  } + (1- \phi)\rho_r c_r
$$
Four derivatives here:
\begin{enumerate}
\item $\frac{\partial \eta_{o} } {\partial T} $, provided by \emph{EOSOilDensityEnergy}.
\item $\frac{\partial U_{o}} {\partial T} $ provided by \emph{EOSOilDensityEnergy}.
\item $\frac{\partial \eta_{w}} {\partial T} $, should be available through EOSWaterDensity, derivs version.
\item $\frac{\partial U_{w} }{\partial T} $, defined in terms of $H_w$, $p_{cell}$, $\eta_w$, fairly straightforwardly. There is a function for $H_w$; there is a deriv option available.
\end{enumerate}
\ldots

\subsection{For Jacobian}
\subsubsection{$u_1$}
$$
\frac{\partial^2 v_1}{\partial u_1^2} = - \frac{\phi s_o \frac{\partial^2 \eta_o }{\partial p_o^2}  }{(\phi s_o \frac{\partial \eta_o}{\partial p_o})^2} =  - \frac{ \frac{\partial^2 \eta_o }{\partial p_o^2}  }{\phi s_o ( \frac{\partial \eta_o}{\partial p_o})^2}
$$
$$
\frac{\partial^2 v_1}{\partial u_1 \partial u_2} =   - \frac{\phi \frac{\partial \eta_o }{\partial p_o}  }{(\phi s_o \frac{\partial \eta_o}{\partial p_o})^2} = - \frac{1 }{\phi s_o^2 \frac{\partial \eta_o}{\partial p_o}}
$$
$$
\frac{\partial^2 v_1}{\partial u_1 \partial u_3} =   - \frac{ \frac{\partial^2 \eta_o }{\partial p_o \partial T}  }{\phi s_o ( \frac{\partial \eta_o}{\partial p_o})^2}
$$
Require the following additional derivatives:
\begin{enumerate}
\item  $ \frac{\partial^2 \eta_o }{\partial p_o^2}$
\item $ \frac{\partial^2 \eta_o }{\partial p_o \partial T} $
\end{enumerate}


\subsubsection{$u_2$}
$$
\frac{\partial^2 v_2}{\partial u_2 \partial u_1}  = +\frac{ \phi  \left(  s_w \frac{\partial^2 \eta_w}{\partial p_{cell} \partial p_o} \frac{\partial p_c}{\partial s_o}    + s_w  \frac{\partial \eta_w}{\partial p_{cell} } \frac{\partial^2 p_c}{\partial s_o \partial p_o} - \frac{\partial \eta_w}{\partial p_o }              \right)  }{ (\ldots)^2 }
$$
$$
\frac{\partial^2 v_2}{\partial u_2 \partial u_2}  =+\frac{ \phi \left( \frac{\partial \eta _w}{\partial p_{cell}}\frac{\partial p_c}{\partial s_o}   - s_w \frac{\partial^2 \eta _w}{\partial p_{cell} \partial s_o}\frac{\partial p_c}{\partial s_o}       - s_w \frac{\partial \eta _w}{\partial p_{cell} }\frac{\partial^2 p_c}{\partial s_o^2}    - \frac{\partial \eta_w}{\partial s_o}     \right)    }{ (\ldots)^2 }
$$
$$
\frac{\partial^2 v_2}{\partial u_2 \partial u_3}  =+\frac{ \phi \left(  - s_w \frac{\partial^2 \eta _w}{\partial p_{cell} \partial T}\frac{\partial p_c}{\partial s_o}       - s_w \frac{\partial \eta _w}{\partial p_{cell} }\frac{\partial^2 p_c}{\partial s_o \partial T}    - \frac{\partial \eta_w}{\partial T}     \right)    }{ (\ldots)^2 }
$$
Require the following additional derivatives:
\begin{enumerate}
\item  $  \frac{\partial^2 \eta_w}{\partial p_{cell} p_o }$
\item $  \frac{\partial^2 p_c}{\partial s_o \partial p_o} $
\item $\frac{\partial^2 \eta _w}{\partial p_{cell} \partial s_o}$
\item $\frac{\partial^2 p_c}{\partial s_o^2}$
\item $ \frac{\partial^2 \eta _w}{\partial p_{cell} \partial T}$
\item $\frac{\partial^2 p_c}{\partial s_o \partial T}   $
\item $\frac{\partial \eta_w}{\partial p_o } $ (along same lines as derive w.r.t. $s_o$).
\item $ \frac{\partial \eta_w}{\partial s_o}   $ (this was already derived).
\end{enumerate}

\subsubsection{$u_3$}
$$
\frac{\partial^2 v_3}{\partial u_3 \partial u_1} = -1 \frac{      \phi \sum_{\alpha}{s_{\alpha} \left( 
%\frac{\partial \eta_{\alpha}  }{\partial T} U_{\alpha} -> 
   \frac{\partial^2 \eta_{\alpha}  }{\partial T \partial p_o} U_{\alpha} 
+ \frac{\partial \eta_{\alpha}  }{\partial T} \frac{ \partial U_{\alpha} }{\partial p_o}
+
%+ \eta_{\alpha}\frac{\partial  U_{\alpha}}{\partial T} ->
   \frac{\partial \eta_{\alpha}}{\partial p_o}  \frac{\partial  U_{\alpha}}{\partial T} 
+ \eta_{\alpha}\frac{\partial^2  U_{\alpha}}{\partial T \partial p_o}
\right)  }     }{(\ldots)^2}
$$

$$
\frac{\partial^2 v_3}{\partial u_3 \partial u_2} = -1 \frac{   \ldots      }{(\ldots)^2}
$$


$$
\frac{\partial^2 v_3}{\partial u_3^2} = -1 \frac{      
\phi \sum_{\alpha}{s_{\alpha} \left( 
%%\frac{\partial \eta_{\alpha}  }{\partial T} U_{\alpha} -> 
   \frac{\partial^2 \eta_{\alpha}  }{\partial T^2 U_{\alpha} }
+ \frac{\partial \eta_{\alpha}  }{\partial T} \frac{ \partial U_{\alpha} }{\partial T}
+
%%+ \eta_{\alpha}\frac{\partial  U_{\alpha}}{\partial T} ->
   \frac{\partial \eta_{\alpha}}{\partial T}  \frac{\partial  U_{\alpha}}{\partial T} 
+ \eta_{\alpha}\frac{\partial^2  U_{\alpha}}{\partial T^2}
\right)  }     
}{(\ldots)^2}
$$

\subsection{Applying the scaling in practice}
Unlike Richardson, TOIL and other MPHASE pms have three ODES per cell, which complicates the scaling. The solution, etc. vectors can be thought of as divided into blocks of three elements for each cell. \\
For the first derivatives and RHS situation, we can do the same dot product type operation as before. Define a
vector 
$$
\frac{\partial \mathbf{v}}{\partial u}
$$
has having blocks of three elements, where for each block the three elements are $\frac{\partial v_i}{\partial u_i}$ for the cell corresponding to that block, then we can simply do 
$$
\mathbf{h} = \frac{\partial \mathbf{v}}{\partial u} \cdot \mathbf{g},
$$
using the PETSC operation. \\
The Jacobian will be slightly more complicated. With regard to \eqref{jac_1}, re-writing to account for the fact that the solution variables are not all $p$, and awkwardly changing the indexing so that it is now over elements of the vectors and not solution variable types (also note should be partial not total derivatives), 
\begin{equation}
\frac{\partial h_i}{\partial v_j} =  \frac{\partial}{\partial v_j}\left[ \frac{d v_i}{d u_i} g_i \right] = \frac{\partial}{\partial v_j}\left[ \frac{d v_i}{d u_i}  \right] g_i + \frac{d v_i}{d u_i}  \frac{\partial g_i}{\partial v_j}.
\label{jac_1_moc}
\end{equation}
It looks like the 
$$
+ \frac{d v_i}{d u_i}  \frac{\partial g_i}{\partial v_j}
$$
part can still be a simple row scaling of the PFLOTRAN Jacobian with $\frac{\partial \mathbf{v}}{\partial u}$. \\
The key difference is that the 
$$
\frac{\partial}{\partial v_j}\left[ \frac{d v_i}{d u_i}  \right]
$$
is no longer a diagonal matrix (or diagonal 3d tensor, or whatever). It will have a very sparse stencil however, due to the functions involved being single-cell. Cross element dependence in the matrix is due only to the fact that \ldots \\
For each row $i$ of the new Jacobian the for this part, clearly $\frac{d v_i}{d u_i} $ and $g_i$ are fixed. The nonzero columns are the ones where $\frac{\partial}{\partial v_j}\left[ \frac{d v_i}{d u_i}  \right]$ is nonzero, of which there are three, and adjacent, since this derivative measures dependence on the three independent variables in the cell $i$ alone. \\
End up with $J'$ being block diagonal, with blocks of $3 \times 3$. Blocks can be easily assembled. 

\section{TOIL Take two}
Clearly the first derivative part for TOIL should be a block diagonal Jacobian, not a simple vector and dot product affair. The Jacobian would be simple to construct or avoid constructing. We are required to compute nine additional derivatives for each cell. \\
To understand constructing the next Jacobian, for the second derivative part, we can use sum notation to (hopefully) avoid dealing with higher order Jacobian, tensor type objects. Consider 
$$
\frac{\partial v_i}{\partial t} = \sum_{k}{g_k \frac{\partial v_i}{\partial u_k}} = \bar{J} \mathbf{g}.
$$
Note the sum is only over three terms. For the next Jacobian we need:
$$
\frac{\partial}{\partial v_j} \left ( \frac{\partial v_i}{\partial t} \right)
$$
$$
= \sum_{k}{\left(          \frac{ \partial g_k}{\partial v_j} \frac{\partial v_i}{\partial u_k}        + g_k     \frac{\partial^2 v_i}{\partial u_k \partial v_j}          \right)}.
$$
This is the i,j th entry of the second order Jacobian we will call $\hat{J}$. Let $\hat{J} = \hat{J}_1 + \hat{J}_2 $, with
$$
(\hat{J}_1)_{i,j} = \sum_{k}{\left(          \frac{ \partial g_k}{\partial v_j} \frac{\partial v_i}{\partial u_k} \right)}
$$
Let $J_{pflotran}$ be the standard pflotran Jacobian with entries $(J_{pflotran})_{k,j} =   \frac{ \partial g_k}{\partial v_j} $, then we have 
$$
\hat{J}_1 = \bar{J}J_{pflotran}.
$$
This generalises the (half) situation in the Richardson solution, where $\bar{J}$ was diagonal and so this part of the computation was replaced with a scaling on the rows of $J_{pflotran}$ (i.e. the effect of multiplication by a diagonal matrix). \\
Next we have to consider 
$$
(\hat{J}_2)_{i,j} = \sum_{k}{\left(   g_k     \frac{\partial^2 v_i}{\partial u_k \partial v_j} \right)}.
$$
Consider a matrix 
$$
(\tilde{J})_{i,j} = \frac{\partial^2 v_i}{\partial u_j \partial v_J}
$$
for some fixed $J$, then
$$
(\tilde{J} \mathbf{g})_{i} =  \sum_{k}{\left(   g_k     \frac{\partial^2 v_i}{\partial u_k \partial v_J} \right)},
$$
i.e., the $J$th column of $\hat{J}_2$. \\
In practice $\tilde{J}$ is of course highly sparse, with nonzero entries only in a $3 \times 3$ block. The structure of $\hat{J}_2$ is inherited from $\bar{J}$. It is an interesting implementation problem to compute each column of $\hat{J}_2$. We will not want to form all of the sparse matrices, clearly. It will likely be a big ugly loop. This will be an excellent area to attempt to apply parallelisation. 

\section{Implementation ideas}
For each of the three `in derivative' equations $u_i$, $i=1,2,3$, it makes sense to collect together all the partial derivative computations in one cell. So define subroutines, one for each $u_i$, which take as input system state (auxvars particularly), and return two arrays:
$$
\bar{j} \equiv
\left( \begin{array}{ccccc}
\frac{\partial u}{\partial v_1} &\frac{\partial u}{\partial v_2} &\frac{\partial u}{\partial v_3}
\end{array} \right)
$$
and
$$
\tilde{j} \equiv
\left( \begin{array}{ccccc}
\frac{\partial^2 u}{\partial v_1^2} &\frac{\partial^2 u}{\partial v_2 \partial v_1} &\frac{\partial^2 u}{\partial v_3 \partial v_1} \\
\frac{\partial^2 u}{\partial v_1 \partial v_2} &\frac{\partial^2 u}{\partial v_2^2} &\frac{\partial^2 u}{\partial v_3 \partial v_2} \\
\frac{\partial^2 u}{\partial v_1 \partial v_3} &\frac{\partial^2 u}{\partial v_2 \partial v_3} &\frac{\partial^2 u}{\partial v_3^2} 
\end{array} \right).
$$
Consider the $i$th ODE on the $n$th cell, corresponding to the $2(n-1) + i$th row, $r$ of the solution vector and so on. Then observe that $\bar{j}$ contains all of the nonzero entries for the $r$th row of $\bar{J}$. \\
Also, letting $g$ be the vector containing the three elements of $\mathbf{g}$ that correspond to cell $n$ ($2(n-1) + i$, $i=1,2,3$), then consider the product $\tilde{j} g$. This vector corresponds to the nonzero elements of the row $i$ of $\hat{J}_2$, although the transpose must be taken since this is a column and not a row vector after the multiplication. \\
One major advantage of grouping together computations for $u_i$ like this is that the many partial derivatives can be done somewhat efficiently, reusing terms, coefficients, etc. as they appear in the 36 very similar computations. \\
A subroutine for computing RHS and JRHS would then build up the needed $\bar{J}$ and $\hat{J}_2$ as follows:
\begin{enumerate}
\item Loop over each cell and over each ODE in the cell.
\item Call the partial deriv tabulation function for $u_i$ appropriate to the ODE, giving the state in the cell as input.
\item Obtain $\bar{j}$ and $\tilde{j} $ as a result.
\item Use these to generate row $r$ of $\bar{J}$ and $\hat{J}_2$.
\end{enumerate}
Note that in a variable switching scenario, there would be logic at part 2) to determine which $u$ partial derivative tabulation function to call based on what the solution variables are; there would be multiple implemented for not only governing ODES but for solution variable sets.


%\section{Current Situation - as of Jan 2017}
%I have implementation of $h$ and the corresponding Jacobian available for Richardson and the EOSs given above. A complete implementation of EEM is also implemented. \\
%When convergence is tested for EEM, it is found to be first order and with a high coefficient of error.  Testing convergence with simple first and second order Taylor type methods, which
%also use $h$ and (in the case of the second order method), the Jacobian, also shows first order convergence. \\
%This indicates that there is something in the construction of $h$ at least, either in implementation or the above theory, that introduces an error of $O(\Delta t)$. I am currently attempting to identify this. If the method
%outlined above were completely wrong, we would see no convergence at all with any scheme making use of $h$, so I believe it is worthwhile continuing to attempt to make this method work. 


\end{document}