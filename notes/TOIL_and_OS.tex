\documentclass{report}



\begin{document}
\section{TOIL formulation revised}
Simplified and corrected, matrix formulation of the TOIL exponential integrator ODE manipulations. \\
Assumptions about $\frac{\partial v}{\partial u}$ being directly available in previous notes were incorrect, we
require an almost identical formulation that uses $\frac{\partial u}{\partial v}^{-1}$ instead. \\
Claims about the form of matrices/tensors here can be checked using elementwise approach seen
in previous notes set. \\

Consider governing ODE:
\begin{equation}
\frac{\partial u(v)}{\partial t} = f(v)
\label{odeu}
\end{equation}
which we want to advance over a timestep $\Delta t$ and solve for $v$. \\
First note that
$$
\frac{\partial u(v)}{\partial t} = \frac{\partial u}{\partial v} \frac{\partial v}{\partial t},
$$
where $\frac{\partial u}{\partial v}$ is a Jacobian matrix, so that
\begin{equation}
\frac{\partial v}{\partial t} = \left( \frac{\partial u}{\partial v}  \right)^{-1} f \equiv g,
\label{odev}
\end{equation}
which is an explicit ODE in terms of $v$ that we can solve using an exponential integrator. \\
For brevity, define 
$$
\frac{\partial u}{\partial v}  \equiv J_{uv},
$$
so that 
\begin{equation}
\frac{\partial v}{\partial t} = \left(  J_{uv}  \right)^{-1} f \equiv g.
\label{odev2}
\end{equation}
We will also need the Jacobian of $g$; let
$$
\frac{\partial g}{\partial v} \equiv J_{gv}. 
$$
To find an expression for $J_{gv}$, we start with
$$
\frac{\partial u}{\partial v} \frac{\partial v}{\partial t} = f,
$$
differentiate both sides by $v$ (elementwise),
$$
\frac{\partial}{\partial v} \left( \frac{\partial u}{\partial v} \frac{\partial v}{\partial t} \right) = \frac{\partial f}{\partial v},
$$
and after applying the product rule and using our definitions of $g$ and $J_{gv}$ we get,
\begin{equation}
\frac{\partial^2 u}{\partial v^2} g + \frac{\partial u}{\partial v} J_{gv} = \frac{\partial f}{\partial v}.
\label{getting-2nd-one}
\end{equation}
We pause here to explain the entity $\frac{\partial^2 u}{\partial v^2}$. It can be thought of as a row vector, whose column entries are the matrices $\frac{\partial }{\partial v_i} J_{uv}$, that is, take the matrix $J_{uv}$ , differentiate every single element by $v_i$, and place the resulting matrix in the $i$th entry of $\frac{\partial^2 u}{\partial v^2}$. \\
Then, the term $\frac{\partial^2 u}{\partial v^2} g $ in (\ref{getting-2nd-one}) is a matrix whose $i$th column is found from the matrix multiplication $\left( \frac{\partial }{\partial v_i} J_{uv} \right) g$. \\
A more detailed elementwise derivation can be done to confirm all this. \\
Returning to (\ref{getting-2nd-one}), we can re-arrange,
$$
J_{gv}  = \left(  J_{uv}  \right)^{-1} \left( \frac{\partial f}{\partial v} - \frac{\partial^2 u}{\partial v^2} g \right).
$$
We can then list the objects that need to be computed to get $g$ and $J_{gv}$:
\begin{itemize}
\item $f$: trivial modification of PFLOTRAN's residual routine.
\item $\frac{\partial f}{\partial v} $: trivial modification of PFLOTRAN's standard Jacobian routine.
\item $J_{uv}^{-1}$: $J_{uv}$ is a block diagonal matrix whose blocks are nothing but the accumulation derivative
                                  blocks calculated by PFLOTRAN, possibly with a factor $\Delta t$ error. These blocks
                                  can be computed and inverted cell by cell and inserted into $J_{uv}^{-1}$.
\item $\frac{\partial }{\partial v_i} J_{uv}$ : these individual matrices are extremely sparse, with nonzero
                                                                 parts being only a block corresponding to the blocks in $J_{uv}$
                                                                 and the cell. Will need to compute up to three (depending on system
                                                                 degrees of freedom) blocks per cell. Can be done numerically
                                                                 and very closely linked to the computation of $J_{uv}$.                                 
\end{itemize}
For a single cell we need to construct:
\begin{itemize}
\item The corresponding block of $J_{uv}$.
\item Derivative of $J_{uv}$, one for each degree
of freedom in the cell. For example for TOIL we need:
\item $\frac{\partial }{\partial p_o} J_{uv}$
\item $\frac{\partial }{\partial s_o} J_{uv}$
\item $\frac{\partial }{\partial T} J_{uv}$
\end{itemize}

\section{operator splitting method}
The idea here is to apply the exponential integrator as directly as possible. Consider again (\ref{odeu}):
\begin{equation}
\frac{\partial u(v)}{\partial t} = f(v)
\end{equation}
we can attempt to advance ${u}$ explicitly using the EI scheme:
$$
{u}^{n+1}  = {u}^{n}  + \Delta t \varphi_1 \left( \frac{\partial {f} }{\partial {u}} \right) {f}.
$$
We then need to obtain ${v}$ at step $n+1$. To do this we can use existing (PETSC) iterative methods. Solve;
$$
\hat{R} \equiv {u}({v}) - {u}^{n+1} = 0.
$$
Now consider the Jacobians that need to be available for this. First for the explicit step, we need
$$
\frac{\partial f}{\partial u} \equiv J_{fu}.
$$
Consider,
$$
J_{fv} = J_{uv} J_{fu} ,
$$
so,
$$
J_{fu}  = J_{uv}^{-1} J_{fv}.
$$
We have $J_{fv}$ as a trivial modification of PFLOTRAN's original Jacobian; and $J_{uv}$ and its inverse
can be constructed blockwise, as discussed in the previous section. 

The other Jacobian needed is for iterative method:
$$
\frac{ \partial \hat{R}}{\partial {v}} = J_{uv} - 0 = J_{uv},
$$
since ${u}^{n+1} $ is constant for this computation. \\
We then need the three Jacobians, $J_{fu}$, $J_{uv}^{-1}$, and $J_{uv}$. $J_{fu}$ is easily obtained by taking the
existing Jacobian routine and removing the accumulation part, and then multiplying by $-1$. $J_{uv}$ and its inverse
are essentially modifications of the existing accumulation derivative routines. Note this method only involves first
order derivatives.

\subsection{Pros and Cons}
Here are the disadvantages which may seem apparent:
\begin{itemize}
\item Naively, it is double the cost, as both and exponential integrator step and an iterative solver are used.
\end{itemize}
A closer look at the objects involved reveals the following potential advantages:
\begin{itemize}
\item The backwards Euler approximation for the time derivative is in effect replaced with and EI approximation 
to deal with the time derivative instead. This is potentially more accurate and stable, and could be almost exact
in the case that $f$ happens to be linear or close to it.
\item The Jacobian used by the iterative solver for this method is always block diagonal, unlike with the standard method. This may make the inverse solves in the iterative solver much more efficient. 
\end{itemize}

\subsection{Implementation Considerations}
\begin{itemize}
\item New residual and Jacobian functions to be created, and assigned to the SNES solver instead of
the regular ones. Involves switching in one of the early setup parts of PFLOTRAN. 
\item The EI step can be abstracted out; it just takes a matrix and a vector. Just construct $f$ and $J_{fu}$.
And feed to existing routines.
\item The StepDT routine will do the following: 1) get RHS and Jacobian for this step. 2) get u for this step.
3) get next value of u by EI call. 4) Call SNES. Note that $u_{n+1}$ is needed by the residual function; so should be
stored in realization or process model something and then accessed by the residual function when computing.
\end{itemize}


\end{document}