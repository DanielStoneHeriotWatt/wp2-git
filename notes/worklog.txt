5/2/17
- derivatives for sat and den w.r.t temp and pres exist but commented out in 
  flow_energy_aux; grandfather to toil_ims_aux, but commented out, clearly WIP
  for Paulo. Reactivated.
- Partially implemented interfaces for returning these derivs found in EOSOil,
  functionallity hidden away by interface that sets derivs option to always 
  false; temp hack since no code exists. Will have to reactivate; either 
  allow some numerical derivs or have automatic numerical option. 
  -> Note PFLOTRAN in general has *no interest* in numerical derivatives here
     since if numerical derivs used, then used in Jacobian, not here. Only point
     of analytical derivs to avoid this. I alone need them for other purposes. 
     -> This if want to contribute working version of this file soon, i.e. to 
        avoid merge problems when someone else works on it, focus on analytical?

4/2/17
- Reactivated derivs in eos_oil. Not tested.
- internal energy derivatives in eos_oil completed. Not sure if I'm missing something; 
  expression is in principle very simple.
- Added more derivatives (internal energy, etc.) in flow_aux. Not sure if some would be better
  off in children/etc classes now that I think about it.
- Note there are dsat/dp etc deriv attributes in flow_aux. Note that in general useful when sat
  is *not* a solution variable (variable switching), but for toil need not worry. Cannot define 
  in toil, and is not needed when partial derivs are of functions that take sat as arg, etc. etc.

19/2/17
Here I note observations about instability in EI implementation (Richards):
  - It's unstable. Pressure tends to blow up. Timestep dependent. Note saturation is ok
    -> seems like it's just set to 1 wherever p is above a threshold.
  - Note sat was used for the promising looking conv tests: errors came from 
    comparing a mostly saturated domain with a completely saturated one.
  - Note that for *greatest* dt, pressure does not explode, and sat agreement with
    compsolve is extremely good.
  - Why is this happening? I conjecture that it is non-adaptivity in the Krylov subspace.
    Can observe Arnoldi alg, for several timesteps near start of run it clearly should have 
    been truncated after one or two itterations. This injects 
